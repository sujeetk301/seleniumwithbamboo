package sampleScripts;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

public class SampleTest {

	WebDriver driver ;
	 @Test
	 public void SampleTest1() {
	   
	     String baseUrl = "https://confluence.atlassian.com/bamboo/";
	       
	          
	         System.out.println("Launching Google Chrome browser"); 
	         driver.get(baseUrl);
	         String testTitle = "Bamboo documentation | Bamboo Server 7.2 | Atlassian Documentation";
	         String originalTitle = driver.getTitle();
	         AssertJUnit.assertEquals(testTitle, originalTitle);
	  }
	 @Test
	 public void SampleTest2() {
		   
	     String baseUrl = "https://www.google.com/";
	       
	          
	         System.out.println("Launching Google Chrome browser"); 
	         driver.get(baseUrl);
	         String testTitle = "Google";
	         String originalTitle = driver.getTitle();
	         AssertJUnit.assertEquals(testTitle, originalTitle);
	         WebElement searchBox= driver.findElement(By.name("q"));
	         searchBox.sendKeys("Test Automation"+Keys.RETURN);
	         String newTitle = driver.getTitle();
	         AssertJUnit.assertEquals("Test Automation - Google Search", newTitle);
	  }
	 
	 @BeforeMethod
	 public void beforeMethod() {
	 System.out.println("Starting Test On Chrome Browser");
	 System.setProperty("webdriver.chrome.driver", "D:/Softwares/chromedriver_win32/chromedriver.exe");
     driver = new ChromeDriver();
	 }
	 
	 @AfterMethod
	 public void afterMethod() {
	 driver.close();
	 System.out.println("Finished Test On Chrome Browser");
	 }
}
